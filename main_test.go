package gomaxproc

import (
	"go.uber.org/automaxprocs/maxprocs"
	"log"
	"runtime"
	"testing"
)

func TestCPUAndGoMaxProcs(t *testing.T) {
	t.Log("CPU number is:", runtime.NumCPU())

	_, _ = maxprocs.Set(maxprocs.Logger(log.Printf))
	t.Log("GOMAXPROCS number is:", runtime.GOMAXPROCS(0))
}
